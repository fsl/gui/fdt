import { useState, useEffect } from 'react'
import './App.css'
import {fsljs} from 'fsljs'

function App() {
  const [versions, setVersions] = useState('')
  async function getVersions(){
    let versions = `FSL version ${await fsljs.fslVersion()}`
    setVersions(versions)
  }

  useEffect(() => {
    getVersions()
  }, [])

  return (
    <>
      <div>
       {versions} 
      </div>
    </>
  )
}

export default App
